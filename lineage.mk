# Inherit device configuration for loki.
$(call inherit-product, device/nvidia/loki/full_loki.mk)

# Inherit some common lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_tv.mk)

PRODUCT_NAME := lineage_loki
PRODUCT_DEVICE := loki
